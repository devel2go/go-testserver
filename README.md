# go-testserver

[![Build Status](https://gitlab.com/devel2go/go-testserver/badges/master/build.svg)](https://gitlab.com/devel2go/go-testserver/commits/master)
[![Coverage Report](https://gitlab.com/devel2go/go-testserver/badges/master/coverage.svg)](https://gitlab.com/devel2go/go-testserver/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/devel2go/go-testserver)](https://goreportcard.com/report/gitlab.com/devel2go/go-testserver)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

Simple HTTP and HTTPS server for testing node deployment.