package main

import (
	"fmt"
	// "io"

	"log"
	"net/http"
	"os"
	"path/filepath"
)

var port string

func helloServer(w http.ResponseWriter, r *http.Request) {
	//log.Print("REQUEST " + req.Method + ": " + req.URL.Path)
	log.Printf("%s %s %s \n", r.Method, r.URL, r.Proto)
	//Iterate over all header fields
	log.Printf("Client Request Header...............")
	for k, v := range r.Header {
		fmt.Printf("Header field %q, Value %q\n", k, v)
	}

	log.Printf("Host = %q\n", r.Host)
	log.Printf("RemoteAddr= %q\n", r.RemoteAddr)
	//Get value for a specified token
	//log.Printf("Finding value of \"Accept\" %q", r.Header["Accept"])
	
	w.Header().Set("Content-Type", "text/plain")
	log.Printf("Server Response Header...............")
	for k, v := range w.Header() {
		fmt.Printf("Header field %q, Value %q\n", k, v)
	}
	w.Write([]byte("This is an example server. Listening on Port: " + port))
	// fmt.Fprintf(w, "This is an example server.\n")
	// io.WriteString(w, "This is an example server.\n")
}

func main() {
	port = "8000"
	if len(os.Args) > 1 {
		port = os.Args[1]
	}
	ex, exErr := os.Executable()
	if exErr != nil {
		log.Fatal(exErr)
	}
	exPath := filepath.Dir(ex)
	http.HandleFunc("/", helloServer)
	//log.Print("HTTP Test Server running on port: " + port)
	log.Print("HTTPS Test Server running on port: " + port)
	
	//  Start HTTP
	//go func() {
		//errHTTP := http.ListenAndServe(":8000", nil)
		//if errHTTP != nil {
			//log.Fatal("Web server (HTTP): ", errHTTP)
		//}
	//}()

	//  Start HTTPS
	errHTTPS := http.ListenAndServeTLS(":" + port, exPath + "/keys/test-server.crt", exPath + "/keys/test-server.key", nil)
	if errHTTPS != nil {
		log.Fatal("Web server (HTTPS): ", errHTTPS)
	}
}
