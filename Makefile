PROJECT_NAME := "go-testserver"
PKG := "gitlab.com/devel2go/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... )
GO_FILES := $(shell find . -name '*.go' | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

## Lint the files
lint:
	@golint -set_exit_status ${PKG_LIST}
	
## Run unittests
test:
	@go test -short ${PKG_LIST}

## Run data race detector
race: dep
	@go test -race -short ${PKG_LIST}

## Run memory sanitizer
msan: dep
	@go test -msan -short ${PKG_LIST}

## Generate global code coverage report
coverage:
	./ztools/coverage.sh;

## Generate global code coverage report in HTML
coverhtml:
	./ztools/coverage.sh html;

## Get the dependencies
dep:
	@go get -v -d ./...
## Build the binary file
build: dep
	@go build -i -v $(PKG)

## Remove previous build
clean:
	@rm -f $(PROJECT_NAME)

## Display this help screen
help:
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
